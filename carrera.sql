-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2021 a las 22:35:43
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carrera`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juego`
--

CREATE TABLE `juego` (
  `id` int(11) NOT NULL,
  `nombre_juego` varchar(3) NOT NULL,
  `participantes` int(11) NOT NULL,
  `pista_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `juego`
--

INSERT INTO `juego` (`id`, `nombre_juego`, `participantes`, `pista_id`) VALUES
(227, '', 5, 2),
(228, '', 5, 2),
(229, '', 5, 2),
(230, '', 5, 2),
(231, '', 5, 2),
(232, '', 5, 2),
(233, '', 5, 2),
(234, '', 5, 2),
(235, '', 5, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pistas`
--

CREATE TABLE `pistas` (
  `nombre` varchar(8) NOT NULL,
  `distancia` int(3) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pistas`
--

INSERT INTO `pistas` (`nombre`, `distancia`, `estado`, `id`) VALUES
('Circuit ', 20, 1, 1),
('Circuit ', 25, 1, 2),
('Circuit ', 10, 1, 3),
('Circuit ', 15, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `podio`
--

CREATE TABLE `podio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(13) NOT NULL,
  `puesto` int(11) NOT NULL,
  `juego_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `podio`
--

INSERT INTO `podio` (`id`, `nombre`, `puesto`, `juego_id`) VALUES
(567, 'Player # 2', 1, 227),
(568, 'Player # 4', 2, 227),
(569, 'Player # 1', 3, 227),
(570, 'Player # 4', 1, 228),
(571, 'Player # 3', 2, 228),
(572, 'Player # 5', 3, 228),
(573, 'Player # 3', 1, 229),
(574, 'Player # 4', 2, 229),
(575, 'Player # 2', 3, 229),
(576, 'Player # 4', 1, 230),
(577, 'Player # 5', 2, 230),
(578, 'Player # 1', 3, 230),
(579, 'Player # 3', 1, 231),
(580, 'Player # 4', 2, 231),
(581, 'Player # 1', 3, 231),
(582, 'Player # 4', 1, 232),
(583, 'Player # 3', 2, 232),
(584, 'Player # 1', 3, 232),
(585, 'Player # 1', 1, 233),
(586, 'Player # 3', 2, 233),
(587, 'Player # 4', 3, 233),
(588, 'Player # 4', 1, 234),
(589, 'Player # 2', 2, 234),
(590, 'Player # 3', 3, 234),
(591, 'Player # 1', 1, 235),
(592, 'Player # 4', 2, 235),
(593, 'Player # 3', 3, 235);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `juego`
--
ALTER TABLE `juego`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pistas`
--
ALTER TABLE `pistas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `podio`
--
ALTER TABLE `podio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `juego`
--
ALTER TABLE `juego`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT de la tabla `pistas`
--
ALTER TABLE `pistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `podio`
--
ALTER TABLE `podio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
