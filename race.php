<html>
 <head>
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <link href="style.css" rel="stylesheet" type="text/css">
     <script src="https://kit.fontawesome.com/a8ffdddeb2.js" crossorigin="anonymous"></script>
 </head>
    <body >

    <div class="container">
        <br><br>
        <h1 CLASS="text-center"><img src="public/sofkau-logo-horizontal.png" width="200px" height="auto"></h1>
        <h1 CLASS="text-center">SCORE TABLE <br></h1>
        <br>

        <div class="row">

            <!-- ITERATIONS -->
            <div class="col">
                <h2 CLASS="text-center">ITERATIONS</h2>
                <div class="results text-center">
                    <?php
                    include 'coneccion.php';

                    $drivers = $_POST['campo1'];
                    $sql = "Insert into juego values(null,'',".$drivers.",".$_POST['pista']." );";
                    $datos = mysqli_query($conn, $sql);

                    $sql = "select * from juego order by id desc limit 1";
                    $datos = mysqli_query($conn, $sql);
                    $juego = mysqli_fetch_array($datos)['id'];

                    $ganadores = array();
                    $jugadores = array();
                    $iteracion=1;

                    while (count($ganadores) < 3){

                        echo "<h3><br>Iteration # ". $iteracion++ . "</h3>";

                        for ($i = 0; $i < $drivers; $i++ ){
                            if (!array_key_exists($i, $jugadores)){
                                $jugadores[$i] = [
                                    "nombre" => "Player # ".($i+1),
                                    "distancia" => 0
                                ];
                            }
                            $jugador = $jugadores[$i];
                            if ( $jugador["distancia"] < $_POST['distancia']){
                                $mts =random_int(1,6)*100;
                                $jugador["distancia"]+= $mts;
                                $jugadores[$i]= $jugador;

                                echo "Player # ". $i+1 . " travelled ". $mts . "  Total: ". $jugador["distancia"] ."<br>";

                                if ( $jugador["distancia"] >= $_POST['distancia'] and (count($ganadores) < 3)){
                                    $ganadores[$i] = $jugador;
                                }
                            }
                        }
                    }

                    echo "<br><br>";

                    ?>
                </div>
            </div>
            <!-- WINNERS -->
            <div class="col">
                <h2 CLASS="text-center"><img src="public/crown-png.png" width="50px" height="auto"></h2>
                <div class="results text-center">
                    <?php
                    $puesto = 1;
                    foreach ($ganadores as $ganador){
                        $sql = "Insert into podio values(null,'".$ganador['nombre']."',".$puesto.",".$juego." );";
                        $datos = mysqli_query($conn, $sql);
                        echo "<h3><br>Place ".$puesto++.": </h3><br>".$ganador['nombre']."<br>";
                    }
                    ?>
                </div>
            </div>
            <!-- PODIUM -->
            <div class="col">
                <h2 CLASS="text-center text-center">PODIUM</h2>
                <div class="results text-center">
                    <?php
                    for ($posicion=1; $posicion<=3; $posicion++){

                        $sql2= "select nombre , count(*) as player from podio WHERE puesto = '".$posicion."' group by nombre;";
                        $datos2 = mysqli_query($conn, $sql2);
                        echo "<h3><br>Top ".$posicion." Winners</h3>";
                        while($row = mysqli_fetch_array($datos2)){
                            echo "Payer: ".$row['nombre']." Has won ".$row['player']." times"."<br>";
                        }

                    }
                    ?>
                </div>
            </div>

            </div>
            <!-- BUTTONS -->
            <div class="btns">
                <?php
                echo "<br>";
                echo "<form> <input value=".'"Play again"'." onclick=".'history.go(0)'." type=".'button'." class='btn-primary' id='btnagain'> </form>";
                echo "<a href=".'index.php'." type=".'button'." class='btn-info white' id='btnvolver'>Back to Home</a>";
                ?>
            </div>

        </div>

    </div>
    </body>
</html>

