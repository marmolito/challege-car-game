<?php
    include 'coneccion.php'
?>

<html lang="es">
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="style.css" rel="stylesheet" type="text/css">
        <script src="https://kit.fontawesome.com/a8ffdddeb2.js" crossorigin="anonymous"></script>
    </head>
    <h1 class="container text-center tittlech">Challenge</h1>

    <body>

    <div class="container " id="challenge">
        <form action="race.php" id="carrera" method="post" class="container">
            <h3>Circuits <br><br></h3>
            <div>
                <?php

                $sql = "SELECT * FROM pistas;";
                $datos = mysqli_query($conn, $sql);

                while($row = mysqli_fetch_array($datos)){
                    echo '<li><input type="radio" name="pista" class="pista" data-distance="'.($row['distancia'] * 1000).'" value="'.$row['id'].'">'.$row['nombre'].' ('.$row['distancia'].'Km) </li>';
                }
                ?>
                <input type="hidden" name="distancia" id="distancia" value="0">
            </div>
            <br>
            <label for="conductores">Type the number of drivers (Min 3, Max 12)</label>
            <input type="number" id="campo1" name="campo1" placeholders="Inserta un dato" min="3" max="12"/>
            <br><br>
            <button type="button" class="btn btn-primary" id='conductores'>List</button>
            <br><br>
            <div id="listadoConductores">

            </div>
            <br>
            <button class="btn btn-info" id="start" style="display: none;">Start</button>
        </form>
    </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        $('#conductores').click(function (){
            var conductores= parseInt($('#campo1').val());
            listarConductores(conductores)
            validarIniciar(conductores)
        });

        $('#carrera').on('submit', function(e){
            // validation code here
            var isValid = validarCampos();
            $("#distancia").val($('.pista:checked').data('distance'));
            return isValid;
        });

       function validarCampos() {
            if($('.pista:checked').length == 0){
                alert("You Should to select a Circuit");
                return false;
            }else if(parseInt($('#campo1').val()) < 3){
                alert("Type minimum 3 players");
                return false;
            }
            return true;
        }

        function listarConductores(conductores){
            $('#listadoConductores').html('');
            for (var i = 0; i < conductores; i++ ){
                $('#listadoConductores').append(`<li>Player # ${i+1}</li>`);
            }
        }

        function validarIniciar(conductores){
            if (conductores > 2)
                $("#start").show();
            else
                $("#start").hide();
        }
    </script>
</html>